const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");

const JWT_SECRET = process.env.JWT_SECRET;

const salt = bcrypt.genSaltSync(10);

const generateToken = user => jwt.sign(JSON.stringify(user), JWT_SECRET);

const hashPwd = password => bcrypt.hashSync(password, salt);

const isValidPassword = (password, user_password) => {
    return new Promise((resolve, reject) => {
        if (!bcrypt.compareSync(password, user_password)) return resolve(false);
        else return resolve(true);
    });
}

const verifyToken = token => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, JWT_SECRET, (err, result) => {
            if (err) return (reject(err));
            return resolve(result);
        });
        
    });
}

const verifyAccount = async (req, res, next) => {
    try {
        const authHeader = req.headers['authorization'];
        const auth_token = authHeader && authHeader.split(' ')[1];
        if (auth_token == null) return res.sendStatus(401);
    
        const token = await verifyToken(auth_token);
        if (!token) return res.sendStatus(401);
    
        req.user = token;
        next();
        
    } catch (error) {
        console.error(error);
        return res.sendStatus(500);
    }
}



module.exports = { generateToken, hashPwd, verifyAccount, isValidPassword }