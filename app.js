require("dotenv");

const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

const app = express();

const user = require('./apis/user');

app.use(fileUpload({ createParentPath: true }));

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/user', user);

app.get("/", (req, res) => res.status(200).json({ status: true, result: 'server is running' }));
app.all("*", (req, res) => res.status(404).json({ error: true, message: 'invalid url' }));

module.exports = app;