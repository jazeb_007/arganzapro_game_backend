const Sequelize = require("sequelize");

const sequelize = require('../config/db.connection');

const db = { Sequelize, sequelize };

Accounts = require('./Accounts')(sequelize, Sequelize);
Assets = require('./Assets')(sequelize, Sequelize);
Levels = require('./Levels')(sequelize, Sequelize);
Enemy = require('./Enemy')(sequelize, Sequelize);
Models = require('./Models')(sequelize, Sequelize);
Catalog = require('./Catalog')(sequelize, Sequelize);
Battery = require('./Battery')(sequelize, Sequelize);


Accounts.hasMany(Assets, {
    foreignKey: 'account_id',
    targetKey:'id'
});

module.exports = { db, Accounts, Assets, Levels, Enemy, Models, Catalog, Battery };
