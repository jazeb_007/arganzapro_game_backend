const mongoose = require('mongoose');

const Accounts = new mongoose.Schema({
    crypto_wallet: {
        type: String,
    },
    client_id: {
        type: String,
    },
    client_token: {
        type: String,
    },
    nickname: {
        type: String,
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
    },
    gmail_connect_id: {
        type: String,
    },
    gmail_token: {
        type: String,
    },
    fb_connect_id: {
        type: String,
    },
    fb_token: {
        type: String,
    },
    account_battery: {
        type: String,
    },
    account_skill_points: {
        type: String,
    },
    account_status: {
        type: String,
        enum: ['Pending', 'Active', 'Deleted', 'Blocked']
    },
    sessions_counter: {
        type: String
    },
    out_of_energy_counter: {
        type: String
    },
    is_social_login: {
        type: Boolean
    },
    social_login_type: {
        type: String
    },
    assets: [
        {
            insert_date: {
                type: Date
            },
            blockchain_id: {
                type: String
            },
            asset_name: {
                type: String
            },
            asset_type: {
                type: String
            },
            
            asset_type: {
                type: String,
                enum: ['Robot', 'Shooting', 'Sword', 'Axe']
            },
            
            asset_status: {
                type: String,
                enum: ['Active', 'InActive']
            },
            
            collective_probability: {
                type: String
            },
            
            IPFS_id: {
                type: String
            },
            
            IPFS_URL: {
                type: String
            },
            video_clip_id: {
                type: String
            },// Fie (360 degrees)
            model_file_3d: {
                type: String
            }, // (the file to be uploaded to the game)
            asset_photo: {
                type: String
            },
            robot_battery_level: {
                type: String
            },
            robot_defending_level: {
                type: String
            },
            robot_attacking_level: {
                type: String
            },
            robot_speed_level: {
                type: Number,
                min: 0,
                max: 100
            },
            total_robot_level: {
                type: String
            },
            shooting_damage: {
                type: Number,
                min: 100,
                max: 300
            },
            shooting_rate: {
                type: Number,
            },
            shooting_cartridge_size: {
                type: Number,
            },
            time_reload_time: {
                type: Number,
                min: 0.1,
                max: 1.5
            }, //(0.1-1.5- sec)
            non_shooting_damage: {
                type: Number,
            },
            non_shooting_metal_strength: {
                type: Number,
                min: 0,
                max: 3
            }, // (1-3)
            non_shooting_movement_speed: {
                type: Number,
                min: 0,
                max: 100
            } //(0-100) 
        }
    ]
}, { collection: 'accounts' });

module.exports = mongoose.model('accounts', Accounts);
