module.exports = function (sequelize, DataTypes) {
    return sequelize.define('assets', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        insert_date: {
            type: DataTypes.STRING(255)
        },
        blockchain_id: {
            type: DataTypes.STRING(255)
        },
        asset_name: {
            type: DataTypes.STRING(255)
        },
        asset_type: {
            type: DataTypes.STRING(255)
        },

        asset_type: {
            type: DataTypes.STRING(10),
            enum: ['Robot', 'Shooting', 'Sword', 'Axe']
        },

        asset_status: {
            type: DataTypes.STRING(10),
            enum: ['Active', 'InActive']
        },

        collective_probability: {
            type: DataTypes.STRING(255)
        },

        IPFS_id: {
            type: DataTypes.STRING(20)
        },

        IPFS_URL: {
            type: DataTypes.STRING(255)
        },
        video_clip_id: {
            type: DataTypes.STRING(255)
        },// Fie (360 degrees)
        model_file_3d: {
            type: DataTypes.STRING(255)
        }, // (the file to be uploaded to the game)
        asset_photo: {
            type: DataTypes.STRING(255)
        },
        robot_battery_level: {
            type: DataTypes.STRING(255)
        },
        robot_defending_level: {
            type: DataTypes.STRING(255)
        },
        robot_attacking_level: {
            type: DataTypes.STRING(255)
        },
        robot_speed_level: {
            type: DataTypes.INTEGER(11),
        },
        total_robot_level: {
            type: DataTypes.STRING(255)
        },
        shooting_damage: {
            type: DataTypes.INTEGER(11)
        },
        shooting_rate: {
            type: DataTypes.INTEGER(11)
        },
        shooting_cartridge_size: {
            type: DataTypes.INTEGER(11),
        },
        time_reload_time: {
            type: DataTypes.INTEGER(11)
        }, //(0.1-1.5- sec)
        non_shooting_damage: {
            type: DataTypes.INTEGER(11),
        },
        non_shooting_metal_strength: {
            type: DataTypes.INTEGER(11)
        }, // (1-3)
        non_shooting_movement_speed: {
            type: DataTypes.INTEGER(11)
        } //(0-100) 

    }, {
        sequelize,
        tableName: 'assets',
        timestamps: false
    });
}