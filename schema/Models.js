module.exports = function (sequelize, DataTypes) {
    return sequelize.define('models', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        model_name: {
            type: DataTypes.STRING(255)
        },
        enemy_status: {
            type: DataTypes.STRING(10),
            enum: ['Active', 'Inactive']
        },
        asset_filename: {
            type: DataTypes.STRING(255)
        },
        asset_filetype: {
            type: DataTypes.STRING(255)
        },
        attacking_level: {
            type: DataTypes.INTEGER(11),
        },
        speed_level: {
            type: DataTypes.INTEGER(11),
        },
        defending_level: {
            type: DataTypes.INTEGER(11),
        },
        hitting_reward: {
            type: DataTypes.INTEGER(11),
        },
        killing_reward: {
            type: DataTypes.INTEGER(11),
        }
    }, {
        sequelize,
        tableName: 'models',
        timestamps: false
    });
}