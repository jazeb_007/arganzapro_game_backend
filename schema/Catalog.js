module.exports = function (sequelize, DataTypes) {
    return sequelize.define('catalog', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        insert_date: {
            type: DataTypes.STRING(20),
        },
        model_id: {
            type: DataTypes.INTEGER(11)
        },
        model_name: {
            type: DataTypes.STRING(20)
        },
        asset_type: {
            type: DataTypes.STRING(11),
            enum: ['Robot', 'Shooting', 'Sword', 'Axe']
        },

        total_ntf_amount: {
            type: DataTypes.INTEGER(11)
        },
        combinations_amount: {
            type: DataTypes.INTEGER(11)
        },
        image_file: {
            type: DataTypes.STRING(100)
        }, 
        video_clip_file:{
            type: DataTypes.STRING(100)
        },
        model_file_3d:{
            type: DataTypes.STRING(100)
        }, 
        sound_file:{
            type: DataTypes.STRING(100)
        },
        IPFS_URL:{
            type: DataTypes.STRING(50)
        }, 
        IPFS_ID:{
            type: DataTypes.STRING(50)
        } 
    }, {
        sequelize,
        tableName: 'catalog',
        timestamps: false
    });
}