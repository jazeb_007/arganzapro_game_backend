module.exports = function (sequelize, DataTypes) {
    return sequelize.define('accounts', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        insert_date: {
            type: DataTypes.STRING(255),
        } ,
        crypto_wallet: {
            type: DataTypes.STRING(255),
            unique: true
        },
        unity_id: {
            type: DataTypes.STRING(255),
        },
        unity_token: {
            type: DataTypes.STRING(255),
        },
        nickname: {
            type: DataTypes.STRING(255),
        },
        token: {
            type: DataTypes.STRING(255),
        },
        client_id: {
            type: DataTypes.STRING(255),
        },
        client_token: {
            type: DataTypes.STRING(255),
        },
        last_account_battery_time: {
            type: DataTypes.STRING(255),
        },
        email: {
            type: DataTypes.STRING(255),
            unique: true,
            required: true,
        },
        password: {
            type: DataTypes.STRING(255),
        },
        gmail_connect_id: {
            type: DataTypes.STRING(255),
        },
        gmail_token: {
            type: DataTypes.STRING(255),
        },
        fb_connect_id: {
            type: DataTypes.STRING(255),
        },
        fb_token: {
            type: DataTypes.STRING(255),
        },
        account_battery: {
            type: DataTypes.STRING(255),
        },
        account_skill_points: {
            type: DataTypes.STRING(255),
        },
        account_status: {
            type: DataTypes.STRING(255),
            enum: ['Pending', 'Active', 'Deleted', 'Blocked']
        },
        sessions_counter: {
            type: DataTypes.STRING(255),
        },
        out_of_energy_counter: {
            type: DataTypes.STRING(255),
        },
        is_social_login: {
            type: DataTypes.BOOLEAN()
        },
        social_login_type: {
            type: DataTypes.STRING(255),
        },
        game_sessions_counter: {
            type: DataTypes.INTEGER(11),
        },
        account_level_no: {
            type: DataTypes.INTEGER(11),
        },
        last_time_unlock_level: {
            type: DataTypes.STRING(20),
        },
        acount_storage: {
            type: DataTypes.INTEGER(11),
        },
        last_time_account_storage_empty: {
            type: DataTypes.STRING(20),
        },
        account_level_points: {
            type: DataTypes.INTEGER(11),
        },
        robot_out_of_energy_counter:{
            type: DataTypes.INTEGER(10)
        },
        account_storage_out_of_energy:{
            defaultValue: 0,
            type: DataTypes.INTEGER(11),
        },
        signin_counter:{
            type: DataTypes.INTEGER(10)
        } 
    }, {
        sequelize,
        tableName: 'accounts',
        timestamps: false
    });
}