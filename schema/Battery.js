module.exports = function (sequelize, DataTypes) {
    return sequelize.define('battery', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        battery_type: {
            type: DataTypes.STRING(22),
        },
        energy_full_level: {
            type: DataTypes.INTEGER(11),
        }
    }, {
        sequelize,
        tableName: 'battery',
        timestamps: false
    });
}