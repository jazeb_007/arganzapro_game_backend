module.exports = function (sequelize, DataTypes) {
    return sequelize.define('enemy', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        insert_date:{
            type: DataTypes.STRING(20)
        },
        enemy_model_id: {
            type: DataTypes.STRING(255)
        },
        enemy_model_name: {
            type: DataTypes.STRING(255)
        },
        enemy_status: {
            type: DataTypes.STRING(10),
            enum: ['Active', 'Inactive']
        },
        image_filename: {
            type: DataTypes.STRING(255)
        },
        video_clip_file: {
            type: DataTypes.STRING(255)
        },
        model_file_3d: {
            type: DataTypes.INTEGER(11),
        },
        sound_file: {
            type: DataTypes.INTEGER(11),
        },
        enemy_defending_level: {
            type: DataTypes.INTEGER(11),
        },
        enemy_attacking_level: {
            type: DataTypes.INTEGER(11),
        },
        enemy_speed_level: {
            type: DataTypes.INTEGER(11),
        },
        hitting_reward: {
            type: DataTypes.INTEGER(11),
        },
        killing_reward: {
            type: DataTypes.INTEGER(11),
        }
    }, {
        sequelize,
        tableName: 'enemy',
        timestamps: false
    });
}