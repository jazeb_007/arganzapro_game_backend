module.exports = function (sequelize, DataTypes) {
    return sequelize.define('levels', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
        },
        level_number: {
            type: DataTypes.INTEGER(11),
        },
        level_points: {
            type: DataTypes.INTEGER(11),
        },
        points_completion: {
            type: DataTypes.INTEGER(11),
        },
        points_multiplier: {
            type: DataTypes.INTEGER(11),
        },
        points_per_klling_multiplier: {
            type: DataTypes.INTEGER(11),
        },
        skills_point_reward: {
            type: DataTypes.INTEGER(11),
        }
    }, {
        sequelize,
        tableName: 'levels',
        timestamps: false
    });
}