require('dotenv').config();

const { PORT } = process.env;
const app = require('./app');

const http = require('http').createServer(app);

http.listen(PORT, _ => console.log(`server is running on port ${PORT}`));