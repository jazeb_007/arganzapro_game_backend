const request = require('request');

const { PORT, URL } = require('./constant');
const baseUrl = `${URL}:${PORT}`;

test('User login', done => {

    let user = {
        email: 'jazeb@gmail.com',
        password: 'password',
        is_social_login: 'false'
    }

    request.post(`${baseUrl}/api/user/login`, {
        form: user
    }, (err, response) => {
        expect(err).toBe(null);
        body = JSON.parse(response.body);
        expect(response.statusCode).toBe(200);
        expect(body.data.user).toBeDefined();
        expect(body.data.token).toBeDefined();
    });
    done();
});