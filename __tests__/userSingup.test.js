const request = require('request');

const { PORT, URL } = require('./constant');
const baseUrl = `${URL}:${PORT}`;

test('User Signup', done => {

    let user = {
        crypto_wallet:'123abbc',
        nickname: 'Jinx',
        email: 'jazeb@gmail.com',
        password: 'password',
        account_status: 'Pending',
        sessions_counter: '10',
        out_of_energy_counter: '100',
        is_social_login: false
    }

    request.post(`${baseUrl}/api/user/signup`, {
        form: user
    }, (err, response) => {
        expect(err).toBe(null);
        body = JSON.parse(response.body);
        expect(response.statusCode).toBe(200);
        expect(body.data.user).toBeDefined();
        expect(body.data.token).toBeDefined();
    });
    done();
});