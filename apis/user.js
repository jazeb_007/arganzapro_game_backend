require('dotenv');
const express = require('express');
const uuidv4 = require('uuid/v4');
const AWS = require('aws-sdk');
const moment = require('moment');
const sequelize = require("sequelize");

const router = express.Router();

const resp = require('../resp');
const { Accounts, Assets, Levels, Enemy, Models, Catalog } = require('../schema/index');

const { generateToken, hashPwd, verifyAccount, isValidPassword } = require('../utils/shared');

const { AccessKeyId, AecretAccessKey, Region } = process.env;

AWS.config.update({
    accessKeyId: AccessKeyId,
    secretAccessKey: AecretAccessKey,
    region: Region
});

const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

// signup and add asset same time
router.post('/addAsset', async (req, res) => {

    try {
        const data = req.body;

        if (!data.crypto_wallet) return resp.error(res, 'Provide wallet id');

        let account = await Accounts.findOne({ where: { crypto_wallet: data.crypto_wallet } });

        if (!account) account = await createAccount(data);

        account = account.toJSON();
        account.account_id = account.id;

        delete account.id;

        await uploadAssets(req, account);

        let include = [{ model: Assets }];

        let user = await Accounts.findOne({ where: { email: data.email }, include });

        return resp.success(res, user);

    } catch (error) {
        console.error(error);
        return resp.error(res, 'Error adding asset', error);
    }

});

function createAccount(data) {
    return new Promise((resolve, reject) => {

        console.log('Account does not exist, creating new one');

        if (!data.email) return reject('Provide email');

        if (!data.is_social_login) return reject('Provide is_social_login true or false');


        if (data.is_social_login === 'true' && !data.social_login_type)
            return reject('Provide social login type');

        if (data.is_social_login === 'false') {
            if (!data.password) return reject('Provide account password');
            data.password = hashPwd(data.password);
        }

        data.account_status = 'Pending';


        Accounts.create(data).then(result => resolve(result)).catch(error => reject(error));

    });
}

// function updateAssetsDB(data) {
//     return new Promise((resolve, reject) => {
//         Assets.create(data).then(result => resolve(result)).catch(err => reject(err));
//     });
// }

async function uploadAssets(req, account) {

    const uploadParams = { Bucket: 'ASSETS_BUCKET', Key: '', Body: '' };

    const files = req.files;
    const fileNames = {};

    for (let [k, file] of Object.entries(files)) {

        let fileType = file.name.replace(' ', '_').split('.').reverse()[0];

        let fileName = k + '_' + uuidv4() + '.' + fileType;

        fileNames[k] = fileName;


        uploadParams.Body = file.data;
        uploadParams.Key = fileName;

        await uploadToS3(uploadParams);
    }
    return fileNames;
};

function uploadToS3(params) {
    return new Promise((resolve, reject) => {
        s3.upload(params, (err, data) => {
            if (err) return reject(err);
            console.log("Upload Success", data.Location);
            return resolve(true);
        });

    });
}

async function checkExistingUser(req, res, next) {

    const data = req.body;
    if (!data.email) return resp.error(res, 'Provide email');

    let account = req.url == '/signup/social'
        ? await Accounts.findOne({ where: { email: data.email, is_social_login: true, social_login_type: req.body.social_login_type } })
        : await Accounts.findOne({ where: { email: data.email, is_social_login: false } });

    console.log({ account })

    if (!account) return next();

    const account_status = account.account_status;

    if (account.account_status == 'Pending') return resp.error(res, { account_status, msg: 'Signup process is not completed' });
    if (account.account_status == 'Active') return resp.error(res, { account_status, msg: 'The account is already active in the system, please click here to sign in' });
    if (account.account_status == 'Blocked') return resp.error(res, { account_status, msg: 'Your account is blocked' });
    if (account.account_status == 'Deleted') return resp.error(res, { account_status, msg: 'This account has been deleted, please contact support for help' });
}

function checkExistingWallet(req, res, next) {
    const data = req.body;
    if (!data.crypto_wallet) return resp.error(res, 'Provide wallet id');

    Accounts.findOne({ where: { crypto_wallet: data.crypto_wallet } }).then(account => {
        if (!account) return next();
        let msg = '';

        if (account.account_status == 'Active') msg = 'The wallet ID is already connected to other account, please use other wallet ID to open a new account';
        else if (account.account_status == 'Blocked') msg = 'This wallet id is Blocked';
        else if (account.account_status == 'Deleted') msg = 'This Wallet ID connected to other account which has been deleted, please contact support for help';
        else if (account.account_status == 'Pending') msg = 'This wallet id connected to another pending account';
        return resp.error(res, msg);

    });
}

function batteryCheck(time) {
    if (user.last_account_battery_time) {

        let endTime = moment().format();
        console.log({ endTime });

        let startTime = moment(user.last_account_battery_time);

        let duration = moment.duration(endTime.diff(startTime));
        let hours = duration.asHours();
        console.log("***********************************************************");
        return hours < 24 ? false : true;
    }
}


function assignInitialValues(req, res, next) {
    req.body.account_level_no = 0;
    req.body.account_storage = 100000;
    req.body.game_sessions_counter = 0;
    req.body.robot_out_of_energy_counter = 0;
    req.body.last_time_unlock_level = '01-01-2000 00:00:00';
    req.body.last_time_account_storage_empty = '01-01-2000 00:00:00';
    next();
}

router.post('/signup/social', checkExistingUser, checkExistingWallet, assignInitialValues, async (req, res) => {
    try {

        const data = req.body;
        console.log("***")

        if (!['FACEBOOK', 'GOOGLE', 'TWITTER'].includes(data.social_login_type)) return resp.error(res, 'Please provide a valid social login type');

        if (!data.token) return resp.error(res, 'Provide social platform token');
        if (!data.nickname) return resp.error(res, 'Nickname is required');


        data.is_social_login = true;
        data.account_status = 'Pending';
        data.password = null;


        let account = await Accounts.create(data);

        const token = generateToken(account);
        console.log(account)
        console.log(token)

        return resp.success(res, { token, account });

    } catch (error) {
        console.error(error);
        return resp.error(res, error);
    }


});

router.post('/signup', checkExistingUser, checkExistingWallet, assignInitialValues, (req, res) => {

    const data = req.body;
    if (!data.password) return resp.error(res, 'Password is required');
    if (!data.nickname) return resp.error(res, 'Nickname is required');

    data.social_login_type = null;
    data.is_social_login = false;
    data.account_status = 'Pending';

    const token = generateToken(data);

    return Accounts.create(data).then(account => resp.success(res, { token, account })).catch(err => resp.error(res, err));

});

router.post('/social/login', (req, res) => {

    try {
        const { email, social_login_type } = req.body;

        if (!email) return resp.error(res, 'Provide email');


        if (!['FACEBOOK', 'GOOGLE', 'TWITTER'].includes(social_login_type)) return resp.error(res, 'Provide social login type');

        let include = [{ model: Assets }];

        Accounts.findOne({ where: { email, is_social_login: true, social_login_type }, include }).then(user => {
            console.log({ user })

            if (!user) return resp.error(res, 'Wrong sign in information, please try again with same way you have created the account”');

            if (user.account_status !== 'Active')
                return resp.error(res, `This account is not active anymore, if you have any questions please contact support for help`);

            if (user.assets.length === 0)
                return resp.error(res, 'Your Account does not have the minimum assets necessary for playing.');

            // implement assets robot time check logic

            let validBatteryTime = true;

            let last_account_battery_time = user.last_account_battery_time;

            if (last_account_battery_time) validBatteryTime = batteryCheck(last_account_battery_time);


            if (!validBatteryTime) return resp.error(res, 'Your account storage is empty, you must wait 24 hours since your storage is over');

            const token = generateToken(user);

            return resp.success(res, { user, token });

        }).catch(err => resp.error(res, err));

    } catch (error) {
        console.error(error);
    }

});


// Login non social account
router.post('/login', (req, res) => {

    const { email, password } = req.body;

    if (!email || !password) return resp.error(res, 'Provide email and password');

    let include = [{ model: Assets }];
    // let user = await Accounts.findOne({ where: { email: data.email },  include } );

    Accounts.findOne({ where: { email, is_social_login: false }, include }).then(async user => {

        if (!user) return resp.error(res, 'Invalid user');

        let isValid = await isValidPassword(password, user.password);

        if (!isValid) return resp.error(res, 'Invalid password');

        if (user.account_status !== 'Active')
            return resp.error(res, `This account is not active anymore, if you have any questions please contact support for help`);

        if (user.assets.length === 0)
            return resp.error(res, 'Your Account does not have the minimum assets necessary for playing');


        let validBatteryTime = true;

        let last_account_battery_time = user.last_account_battery_time;

        if (last_account_battery_time) validBatteryTime = batteryCheck(last_account_battery_time);


        if (!validBatteryTime) return resp.error(res, 'Your account storage is empty, you must wait 24 hours since your storage is over');

        // implement assets robot time check logic

        const token = generateToken(user);

        return resp.success(res, { user, token });

    }).catch(err => resp.error(res, err));
});


router.put('/game/end', async (req, res) => {

    // update accounts table
    // update assets table
    // update robot battery level

    const data = req.body;
    const account_id = req.user.id;
    const { asset_id } = data;

    if (!asset_id) return resp.error(res, 'Provide asset_id');

    delete data.id;
    delete data.asset_id;
    delete data.email;
    delete data.password;


    await Assets.update(data, { where: { id: asset_id } });
    await Accounts.update({ account_level_points: sequelize.literal('account_level_points + ' + account_level_points) }, { where: { id: account_id } });
    await Accounts.update({ robot_out_of_energy: sequelize.literal('robot_out_of_energy + 1') }, { where: { id: account_id } });

    return resp.success(res, 'Game stats updated');
});


router.put('/level/complete', verifyAccount, async (req, res) => {


    const data = req.body;
    const account_id = req.user.id;
    const { asset_id } = data;

    if (!asset_id) return resp.error(res, 'Provide asset_id');

    delete data.id;
    delete data.asset_id;

    await Accounts.update(data, { where: { id: account_id } });
    await Assets.update(data, { where: { id: asset_id } });

    let user = await Accounts.findOne({ where: { id: account_id } });
    let levels = await Levels.findAll();


    levels.length > 0 && levels.sort((a, b) => a.level_number - b.level_number);
    levels = levels.pop();

    if (levels && user.account_level_no >= levels.level_number) return res.error(res, "No more levels, new levels coming soon, meanwhile continue playing");
    return resp.success(res, 'Game levels updated');

});


router.put('/game/charge/robotBattery', verifyAccount, async (req, res) => {


    const body = req.body;
    const account_id = req.user.id;
    console.log({ account_id })

    if (!body.asset_id) return resp.error(res, 'Provide asset id of robot');


    await Accounts.update(body, { where: { id: account_id } });
    // await Assets.update(body, { where: { id: body.asset_id }});

    const user = await Accounts.findOne({ where: { id: account_id } });

    if (user.account_storage_out_of_energy == 0) {

        await Accounts.update({ account_storage_out_of_energy: sequelize.literal('account_storage_out_of_energy + 1') }, { where: { id: account_id } });

        let time = moment().format();

        await Accounts.update({ last_account_battery_time: time }, { where: { id: account_id } });

        return resp.success(res, 'Account storage and battery time updated');

    }

});


// Enemy routes
router.post('/enemy', (req, res) => {
    const body = req.body;
    Enemy.create(body).then(enemy => resp.success(res, enemy)).catch(err => resp.error(res, err));
});

router.get('/enemy', (req, res) => {
    Enemy.findAll().then(enemy => resp.success(res, enemy)).catch(err => resp.error(res, err));
});

router.put('/enemy', (req, res) => {
    const body = req.body;
    enemy_id = body.id;

    delete body.id;
    Enemy.update(body, { where: { id: enemy_id } }).then(_ => resp.success(res, 'Enemy updated successfully')).catch(err => {
        console.log(err);
        return resp.error(res, err);
    });
});

// Models routes
router.get('/model', (req, res) => {
    Models.findAll().then(models => resp.success(res, models)).catch(err => resp.error(res, err));
});


router.post('/model', (req, res) => {
    const body = req.body;
    Models.create(body).then(model => resp.success(res, model)).catch(err => resp.error(res, err));
});

router.put('/model', (req, res) => {
    const body = req.body;
    model_id = body.id;

    delete body.id;
    Models.update(body, { where: { id: model_id } }).then(_ => resp.success(res, 'Model updated successfully')).catch(err => {
        console.log(err);
        return resp.error(res, err);
    });
});

// Catalog routes
router.get('/catalog', (req, res) => {
    Catalog.findAll().then(catalog => resp.success(res, catalog)).catch(err => resp.error(res, err));
});


router.post('/catalog', async (req, res) => {
    const body = req.body;
    let fileNames = await uploadAssets(req);

    for (let [k, v] of Object.entries(fileNames)) body[k] = v;

    Catalog.create(body).then(catalog => resp.success(res, catalog)).catch(err => resp.error(res, err));
});

router.put('/catalog', (req, res) => {
    const body = req.body;
    if (!body.id) return resp.error(res, 'Provide id to update');

    catalog_id = body.id;

    delete body.id;
    Catalog.update(body, { where: { id: catalog_id } }).then(_ => resp.success(res, 'Catalog updated successfully')).catch(err => {
        console.log(err);
        return resp.error(res, err);
    });
});

// Levels routes
router.get('/level', (req, res) => {
    Levels.findAll().then(level => resp.success(res, level)).catch(err => resp.error(res, err));
});


router.post('/level', (req, res) => {
    const body = req.body;
    Levels.create(body).then(level => resp.success(res, level)).catch(err => resp.error(res, err));
});

router.put('/level', (req, res) => {
    const body = req.body;
    if (!body.id) return resp.error(res, 'Provide id to update');

    level_id = body.id;

    delete body.id;
    Levels.update(body, { where: { id: level_id } }).then(_ => resp.success(res, 'Level updated successfully')).catch(err => {
        console.log(err);
        return resp.error(res, err);
    });
});

// Battery routes
router.get('/battery', (req, res) => {
    Battery.findAll().then(battery => resp.success(res, battery)).catch(err => resp.error(res, err));
});


router.post('/battery', (req, res) => {
    const body = req.body;
    Battery.create(body).then(battery => resp.success(res, battery)).catch(err => resp.error(res, err));
});

router.put('/battery', (req, res) => {
    const body = req.body;
    if (!body.id) return resp.error(res, 'Provide id to update');
    battery_id = body.id;

    delete body.id;
    Battery.update(body, { where: { id: battery_id } }).then(_ => resp.success(res, 'Battery data updated successfully')).catch(err => {
        console.log(err);
        return resp.error(res, err);
    });
});



module.exports = router;



/************
 * async function uploadAssets(req, account) {

    // const uploadParams = { Bucket: 'ASSETS_BUCKET', Key: '', Body: '' };

    const files = req.files;
    const fileNames = {};
    console.log(req.files)

    for (let [k, file] of Object.entries(files)) {


            let fileType = file.name.replace(' ', '_').split('.').reverse()[0];

            let fileName =  k + '_' + uuidv4() + '.' + fileType;

            console.log({ fileName });

            fileNames[k] = fileName;


            // uploadParams.Body = file.data;
            // uploadParams.Key = fileName;

            // account.asset_name = fileName;
            // account.asset_type = k;


            // await uploadToS3(uploadParams);
            // await updateAssetsDB(account);
        // for (let file of v) {}
    }
    return fileNames;
};
 */